
typedef float DTYPE;
#define SIZE 256 		/* SIZE OF DFT */
#define pi (3.141592653589)
void dft(DTYPE X_R[SIZE], DTYPE X_I[SIZE],DTYPE Y_R[SIZE], DTYPE Y_I[SIZE]);
//void dft(DTYPE X_R[SIZE], DTYPE X_I[SIZE]);//Solution1 naive baseline

