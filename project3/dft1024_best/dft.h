#ifndef DFT_H
#define DFT_H

//#include "coefficients256.h"
typedef float DTYPE;
#define SIZE 1024 		/* SIZE OF DFT */
#define pi (3.141592653589)
#define UNROLL_FACTOR 8
void dft(DTYPE X_R[SIZE], DTYPE X_I[SIZE],DTYPE Y_R[SIZE], DTYPE Y_I[SIZE]);
//void dft(DTYPE X_R[SIZE], DTYPE X_I[SIZE]);//Solution1 naive baseline

//void get_coefs(DTYPE c[UNROLL_FACTOR], DTYPE s[UNROLL_FACTOR], int);
void get_coefs(DTYPE *, DTYPE *, int, int);

void dft_subvec_acc(DTYPE *, DTYPE *, DTYPE, DTYPE , DTYPE *, DTYPE *);

void out_put(DTYPE *, DTYPE *, DTYPE *,DTYPE *, int );

#endif
