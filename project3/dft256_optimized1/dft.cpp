#include<math.h>//#include<hls_math.h>//
#include<stdio.h>
#include "dft.h"
#include"coefficients256.h"
void dft(DTYPE real_sample[SIZE], DTYPE imag_sample[SIZE])//comment-in for Solution1: baseline naive
//void dft(DTYPE real_sample[SIZE], DTYPE imag_sample[SIZE], DTYPE real_out[SIZE], DTYPE imag_out[SIZE])//comment-in for Solution3:
{
	//Write your code here
	DTYPE real_out[SIZE];
	DTYPE imag_out[SIZE];//comment-in for Solution1: baseline naive, and Solution2: LUT instead of math calls

	double w, wn;//comment-in for Solution1: baseline naive
	DTYPE c, s;
	unsigned int ip=0;

			//X_k = X_n*e^(-i*2*pi*k*n/N)
dft_label0:for (int k=0; k<SIZE; k++) {
				DTYPE real_acc=0;
				DTYPE imag_acc=0;
				w=(2.0*pi*(DTYPE)k/(DTYPE)SIZE);//comment-in for Solution1: baseline naive
				for (int n =0; n<SIZE; n++)
				{
					/*wn=w*n;

					c=cos(-wn);
					s=sin(-wn);*///comment-in for Solution1: baseline naive

					c=cos_coefficients_table[k*n%SIZE];
					s=sin_coefficients_table[k*n%SIZE];//comment-in for Solution2: LUT instead of math calls

					/*ip=n==0?0:(ip+k)%SIZE;

					c=cos_coefficients_table[ip];
					s=sin_coefficients_table[ip];*///comment-in for Solution3: restruct. + separate output array

					real_acc+=real_sample[n]*c-imag_sample[n]*s;
					//real_acc-=imag_sample[n]*s;
					imag_acc+=imag_sample[n]*c+real_sample[n]*s;
					//imag_acc+=real_sample[n]*s;
				}

				real_out[k]=real_acc;//leave-in for Solution3: separate output array
				imag_out[k]=imag_acc;//leave-in for Solution3: separate output array
			}
			for (int k=0; k<SIZE; k++)
			{//comment-in for Solution1: baseline naive
				real_sample[k]=real_out[k];
				imag_sample[k]=imag_out[k];
			}
}
