############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project dft1024_bonus
set_top dft
add_files dft.h
add_files dft.cpp
add_files coefficients1024.h
add_files -tb out.gold.dat
add_files -tb dft_test.cpp
open_solution "dft1024_solution_bonus"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
#source "./dft1024_bonus/dft1024_solution_bonus/directives.tcl"
csim_design -clean
csynth_design
cosim_design
export_design -format ip_catalog
