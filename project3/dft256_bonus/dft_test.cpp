
#include<stdio.h>
#include <stdlib.h>
#include<iostream>
#include <math.h>
#include "hls_stream.h"
#include "dft.h"

struct Rmse
{
	int num_sq;
	float sum_sq;
	float error;

	Rmse(){ num_sq = 0; sum_sq = 0; error = 0; }

	float add_value(float d_n)
	{
		num_sq++;
		sum_sq += (d_n*d_n);
		error = sqrtf(sum_sq / num_sq);
		return error;
	}

};


Rmse rmse_R,  rmse_I;
//Added for hls::stream :Prasant Samal

hls::stream<DTYPE> In_R ("input_R");
hls::stream<DTYPE> In_I ("input_I");

hls::stream<DTYPE> Out_R ("output_R");
hls::stream<DTYPE> Out_I ("output_I");

int main()
{
	int index;
	DTYPE gold_R, gold_I;

	FILE * fp = fopen("out.gold.dat","r");


	// getting input data
	for(int i=0; i<SIZE; i++)
	{
		In_R << i;
		In_I << 0.0;
	}


	dft(In_R, In_I,Out_R,Out_I);



	for(int i=0; i<SIZE; i++)
	{
		DTYPE l_R = Out_R.read();
		DTYPE l_I = Out_I.read();
		printf("%d %f %f\n", i, l_R,l_I);
		fscanf(fp, "%d %f %f", &index, &gold_R, &gold_I);
		rmse_R.add_value((float)l_R - gold_R);
		rmse_I.add_value((float)l_I - gold_I);
	}
	fclose(fp);


	// printing error results
	printf("----------------------------------------------\n");
	printf("   RMSE(R)           RMSE(I)\n");
	printf("%0.15f %0.15f\n", rmse_R.error, rmse_I.error);
	printf("----------------------------------------------\n");

	if (rmse_R.error > 0.1 || rmse_I.error > 0.1 ) {
		fprintf(stdout, "*******************************************\n");
		fprintf(stdout, "FAIL: Output DOES NOT match the golden output\n");
		fprintf(stdout, "*******************************************\n");
	    return 1;
	}else {
		fprintf(stdout, "*******************************************\n");
		fprintf(stdout, "PASS: The output matches the golden output BONUS!\n");
		fprintf(stdout, "*******************************************\n");
	    return 0;
	}

}
