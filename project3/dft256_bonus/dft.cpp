
#include "hls_stream.h"
//Models for streaming data structures.  Designed to obtain best performance and area (hls_stream.h)
#include "dft.h"
#include"coefficients256.h"


#define UNROLL_FACTOR_1 32
//Out
#define UNROLL_FACTOR_2 32
//In


template <int copy_id>
DTYPE get_cos(int index)
{
	return cos_coefficients_table[index%SIZE];
}

template <int copy_id>
DTYPE get_sin(int index)
{
	return sin_coefficients_table[index%SIZE];
}



template <int copy_id>
void get_c(DTYPE cos_coef[UNROLL_FACTOR_1][UNROLL_FACTOR_2], DTYPE sin_coef[UNROLL_FACTOR_1][UNROLL_FACTOR_2],const int xx, const int yy)
{
	int base = yy*xx;


    coef_loop_in : for(int unroll_in =0; unroll_in < UNROLL_FACTOR_2; unroll_in++) {
    	int index=base;
		coef_loop_out: for (int unroll = 0; unroll<UNROLL_FACTOR_1; unroll++)
		{
	#pragma HLS PIPELINE
			cos_coef[unroll][unroll_in]=get_cos<copy_id>(index);
			sin_coef[unroll][unroll_in]=get_sin<copy_id>(index);
			index+=(yy+unroll_in);
		}
		base+=xx;
    }
}


inline DTYPE dft_i_real(DTYPE cos_coef, DTYPE _sin_coef,DTYPE real_in, DTYPE imag_in)
{
	return real_in*cos_coef - imag_in*_sin_coef;
}
inline DTYPE dft_imag(DTYPE cos_coef, DTYPE _sin_coef,DTYPE real_in, DTYPE imag_in)
{
	return imag_in*cos_coef + real_in*_sin_coef;
}

template <int copy_id>
void dft_inc(DTYPE cos_coef[UNROLL_FACTOR_1][UNROLL_FACTOR_2], DTYPE sin_coef[UNROLL_FACTOR_1][UNROLL_FACTOR_2],
			hls::stream<DTYPE> &real_in, hls::stream<DTYPE> &imag_in,
			DTYPE a_r[UNROLL_FACTOR_1], DTYPE a_i[UNROLL_FACTOR_1])
{

	for(int unroll_in =0; unroll_in < UNROLL_FACTOR_2; unroll_in++) {
#pragma HLS LOOP_FLATTEN
#pragma HLS PIPELINE
		DTYPE m_real_in = real_in.read();
		DTYPE m_imag_in = imag_in.read();

		for (int unroll = 0; unroll<UNROLL_FACTOR_1; unroll++) {

			DTYPE m_cos = cos_coef[unroll][unroll_in];
			DTYPE m_sin = sin_coef[unroll][unroll_in];
			a_r[unroll]+=dft_i_real(m_cos,m_sin,m_real_in,m_imag_in);
			a_i[unroll]+=dft_imag(m_cos,m_sin,m_real_in,m_imag_in);
		}
	}
}



template <int copy_id>
void dft_m(hls::stream<DTYPE> &r_s, hls::stream<DTYPE> &i_s,
				hls::stream<DTYPE> &a_r_out, hls::stream<DTYPE> &a_i_out, int xx) {
	DTYPE a_r[UNROLL_FACTOR_1]={0};
	DTYPE a_i[UNROLL_FACTOR_1]={0};

	input_index_loop:for (int yy =0; yy<SIZE; yy+= UNROLL_FACTOR_2)
	{
#pragma HLS DATAFLOW
		DTYPE cos_coef[UNROLL_FACTOR_1][UNROLL_FACTOR_2], sin_coef[UNROLL_FACTOR_1][UNROLL_FACTOR_2];
#pragma HLS STREAM variable=cos_coef dim=2
#pragma HLS STREAM variable=sin_coef dim=2
		get_c<copy_id>(cos_coef,sin_coef,xx,yy);
		dft_inc<copy_id>(cos_coef,sin_coef,r_s,i_s,a_r,a_i);
	}

	for (int yy=0; yy<UNROLL_FACTOR_1;yy++) {
		a_r_out << a_r[yy];
		a_i_out << a_i[yy];
	}
}

#define computation 1

void outp(hls::stream<DTYPE> a_r[computation],
		hls::stream<DTYPE> a_i[computation],
		hls::stream<DTYPE> &r_out, hls::stream<DTYPE> &i_out,const int xx)
{

	for (int compute = 0; compute < computation; compute++) {
#pragma pipeline
		for (int unroll = 0; unroll<UNROLL_FACTOR_1; unroll++) {
			r_out << a_r[compute].read();
			i_out << a_i[compute].read();
		}
	}
}
#define DO_PRAGMA(x) _Pragma (#x)
#define STREAM(x,y) DO_PRAGMA (HLS STREAM variable=x depth=y )
void dft(hls::stream<DTYPE> &r_s_in, hls::stream<DTYPE> &i_s_in,
		hls::stream<DTYPE> &r_out, hls::stream<DTYPE> &i_out)
{
STREAM (r_s_in, SIZE)
STREAM (i_s_in, SIZE)
STREAM (r_out, SIZE)
STREAM (i_out, SIZE)
#pragma HLS STREAM variable=r_s_in depth=256 dim=1

#pragma HLS DATAFLOW

	hls::stream<DTYPE> r_s[computation];
	hls::stream<DTYPE> i_s[computation];

	DTYPE real[SIZE];
	DTYPE imag[SIZE];
	for (int j=0;j<SIZE;j++) {
		 real[j] = r_s_in.read();
		 imag[j] = i_s_in.read();
	}


#pragma HLS ARRAY_PARTITION variable=i_s complete dim=1
#pragma HLS ARRAY_PARTITION variable=r_s complete dim=1

	for (int xx=0; xx<SIZE; xx+=UNROLL_FACTOR_1*computation) {
		hls::stream<DTYPE> a_r[computation];
		hls::stream<DTYPE> a_i[computation];
#pragma DATAFLOW
STREAM (a_r, UNROLL_FACTOR_1*2)
STREAM (a_i, UNROLL_FACTOR_1*2)
#pragma HLS ARRAY_PARTITION variable=a_r complete dim=1
#pragma HLS ARRAY_PARTITION variable=a_i complete dim=1


#if computation == 1
		for (int j=0;j<SIZE;j++) {
				r_s[0] << real[j];
				i_s[0] << imag[j];
STREAM (r_s, UNROLL_FACTOR_2*2)
STREAM (i_s, UNROLL_FACTOR_2*2)

		}
		dft_m<1>(r_s[0], i_s[0], a_r[0], a_i[0], xx);
#else
		dft_m<1>(r_s[0], i_s[0], a_r[0], a_i[0], xx);
#if computation > 1
		dft_m<2>(r_s[1], i_s[1], a_r[1], a_i[1], xx+UNROLL_FACTOR_1*1);
#if computation > 2
		dft_m<3>(r_s[2], i_s[2], a_r[2], a_i[2], xx+UNROLL_FACTOR_1*2);
#if computation > 3
		dft_m<4>(r_s[3], i_s[3], a_r[3], a_i[3], xx+UNROLL_FACTOR_1*3);

#endif
#endif
#endif
#endif


		outp(a_r,a_i,r_out,i_out,xx);
	}
}

