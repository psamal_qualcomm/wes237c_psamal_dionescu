
typedef float DTYPE;
#define SIZE 256
//  Changes for hls::Stream :Prasant
void dft(hls::stream<DTYPE> &XX_R, hls::stream<DTYPE> &XX_I,
		hls::stream<DTYPE> &O_R, hls::stream<DTYPE> &O_I);

