#include<math.h>//#include<hls_math.h>//
#include<stdio.h>
#include "dft.h"
#include "coefficients256.h"


void get_coefs(DTYPE c[UNROLL_FACTOR], DTYPE s[UNROLL_FACTOR], int ip, int n)
{
	int v=UNROLL_FACTOR;
	coef_subset: for (int unrolled=0; unrolled<UNROLL_FACTOR; ip=(ip+n)%SIZE, unrolled++){

#pragma HLS PIPELINE
#pragma HLS UNROLL factor=v
		//ip=unrolled==0?ip:(ip+n)%SIZE;
		c[unrolled]=cos_coefficients_table[ip];
		s[unrolled]=sin_coefficients_table[ip];
	}

}


void dft_subvec_acc(DTYPE c[UNROLL_FACTOR], DTYPE s[UNROLL_FACTOR], DTYPE real_in, DTYPE imag_in, DTYPE acc_real[UNROLL_FACTOR], DTYPE acc_imag[UNROLL_FACTOR])
{
	int v=UNROLL_FACTOR;
	for (int unrolled=0; unrolled<UNROLL_FACTOR; unrolled++){
#pragma HLS UNROLL factor=v
#pragma HLS PIPELINE
		acc_real[unrolled]+=real_in*c[unrolled]-imag_in*s[unrolled];
		acc_imag[unrolled]+=imag_in*c[unrolled]+real_in*s[unrolled];
	}

}

void out_put(DTYPE acc_real[UNROLL_FACTOR], DTYPE acc_imag[UNROLL_FACTOR], DTYPE real_out[SIZE], DTYPE imag_out[SIZE], int k)
{
	int v=UNROLL_FACTOR;
	for (int unrolled=0; unrolled<UNROLL_FACTOR; unrolled++){
#pragma HLS UNROLL factor=v
#pragma HLS PIPELINE
		int n=unrolled+k;
		real_out[n]=acc_real[unrolled];
		imag_out[n]=acc_imag[unrolled];
	}

}

void dft(DTYPE real_sample[SIZE], DTYPE imag_sample[SIZE], DTYPE real_out[SIZE], DTYPE imag_out[SIZE])//comment-in for Solution3:
{
#pragma HLS DATAFLOW
	//Write your code here
	int v=UNROLL_FACTOR;
//#pragma HLS ARRAY_PARTITION variable=cos_coefficients_table factor=v dim=1
//#pragma HLS ARRAY_PARTITION variable=sin_coefficients_table factor=v dim=1
#pragma HLS ARRAY_PARTITION variable=real_sample cyclic factor=v dim=1
#pragma HLS ARRAY_PARTITION variable=imag_sample cyclic factor=v dim=1
#pragma HLS ARRAY_PARTITION variable=real_out cyclic factor=v dim=1
#pragma HLS ARRAY_PARTITION variable=imag_out cyclic factor=v dim=1


#pragma HLS UNROLL factor=v

	//DTYPE c, s;

	unsigned int ip=0;

			//X_k += x_n*e^(-i*2*pi*k*n/N)
dft_rowOut:for (int k=0; k<SIZE; k+=UNROLL_FACTOR) {
				DTYPE real_acc[UNROLL_FACTOR]={0};
				DTYPE imag_acc[UNROLL_FACTOR]={0};
#pragma HLS ARRAY_PARTITION variable=real_acc complete
#pragma HLS ARRAY_PARTITION variable=imag_acc complete
				dft_colOut:for (int n =0; n<SIZE; n++)
				{

#pragma HLS PIPELINE

					DTYPE c[UNROLL_FACTOR], s[UNROLL_FACTOR];
#pragma HLS ARRAY_PARTITION variable=c complete
#pragma HLS ARRAY_PARTITION variable=s complete
					//ip=n==0?0:(ip+k)%SIZE;
					ip=(k*n)%SIZE;

					get_coefs(c, s, ip, n);
					dft_subvec_acc(c,s,real_sample[n],imag_sample[n],real_acc,imag_acc);

					/*  real_acc+=real_sample[n]*c[0]-imag_sample[n]*s[0];
					//real_acc-=imag_sample[n]*s;
					imag_acc+=imag_sample[n]*c[0]+real_sample[n]*s[0];
					//imag_acc+=real_sample[n]*s;  */
				}
				out_put(real_acc,imag_acc,real_out,imag_out,k);

				/*  real_out[k]=real_acc;//leave-in for Solution3: separate output array
				imag_out[k]=imag_acc; *///leave-in for Solution3: separate output array
			}

}
