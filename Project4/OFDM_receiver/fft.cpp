/*
This is traditional 2-radix DIT FFT algorithm implementation.
INPUT:
	In_R, In_I[]: Real and Imag parts of Complex signal

OUTPUT:
	Out_R, Out_I[]: Real and Imag parts of Complex signal
*/

#include "fft.h"

void bit_reverse(DTYPE X_R[SIZE], DTYPE X_I[SIZE],DTYPE O_R[SIZE], DTYPE O_I[SIZE]);
void fft_stage_first(DTYPE X_R[SIZE], DTYPE X_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]);
void fft_stages(DTYPE X_R[SIZE], DTYPE X_I[SIZE], int STAGES, DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]);
void fft_stage_last(DTYPE X_R[SIZE], DTYPE X_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]);
void cp(DTYPE X_R[SIZE], DTYPE X_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]);

void fft(DTYPE X_R[SIZE], DTYPE X_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE])
{
#pragma HLS DATAFLOW
//#pragma HLS ARRAY_PARTITION variable=W_real cyclic factor=8 dim=1
//#pragma HLS ARRAY_PARTITION variable=W_imag cyclic  factor=8 dim=1
	//Call fft
	DTYPE Stage0_R[SIZE], Stage0_I[SIZE];
	DTYPE Stage1_R[SIZE], Stage1_I[SIZE];
	DTYPE Stage2_R[SIZE], Stage2_I[SIZE];
	DTYPE Stage3_R[SIZE], Stage3_I[SIZE];
	DTYPE Stage4_R[SIZE], Stage4_I[SIZE];
	DTYPE Stage5_R[SIZE], Stage5_I[SIZE];
	DTYPE Stage6_R[SIZE], Stage6_I[SIZE];
	DTYPE Stage7_R[SIZE], Stage7_I[SIZE];
	DTYPE Stage8_R[SIZE], Stage8_I[SIZE];
	DTYPE Stage9_R[SIZE], Stage9_I[SIZE];

	bit_reverse(X_R, X_I, Stage0_R, Stage0_I);

	fft_stage_first(Stage0_R, Stage0_I, Stage1_R, Stage1_I);
	fft_stages(Stage1_R, Stage1_I, 2, Stage2_R, Stage2_I);
	fft_stages(Stage2_R, Stage2_I, 3, Stage3_R, Stage3_I);
	fft_stages(Stage3_R, Stage3_I, 4, Stage4_R, Stage4_I);
	fft_stages(Stage4_R, Stage4_I, 5, Stage5_R, Stage5_I);
	fft_stages(Stage5_R, Stage5_I, 6, Stage6_R, Stage6_I);
	fft_stages(Stage6_R, Stage6_I, 7, Stage7_R, Stage7_I);
	fft_stages(Stage7_R, Stage7_I, 8, Stage8_R, Stage8_I);
	fft_stages(Stage8_R, Stage8_I, 9, Stage9_R, Stage9_I);
	fft_stage_last(Stage9_R, Stage9_I, OUT_R, OUT_I);

}

#define FFT_BITS 10	// Number of bits required to index FFT samples, log(1024)

void cp(DTYPE X_R[SIZE], DTYPE X_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]){
#pragma HLS PIPELINE
	cp_loop: for (int i = 0; i < SIZE; i++) {
		OUT_R[i] = X_R[i];
		OUT_I[i] = X_I[i];
	}
}


unsigned int reverse_bits(unsigned int input){
#pragma HLS INLINE
	int i, rev = 0;

	for(i = 0; i<FFT_BITS; i++){
#pragma HLS UNROLL //factor=2
		rev = (rev << 1)|(input & 1);
		input = input >> 1;
	}
	return rev;
}

void bit_reverse(DTYPE X_R[SIZE], DTYPE X_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]){
//#pragma HLS ALLOCATION instances=reverse_bits limit=1 function
	//Write your code here.
/* #pragma HLS ARRAY_PARTITION variable=X_R cyclic factor=512 dim=1
#pragma HLS ARRAY_PARTITION variable=X_I cyclic factor=512 dim=1
#pragma HLS ARRAY_PARTITION variable=OUT_R cyclic factor=512 dim=1
#pragma HLS ARRAY_PARTITION variable=OUT_I cyclic factor=512 dim=1*/
	unsigned int reversed;
	unsigned int i;
	DTYPE temp;
//#pragma HLS PIPELINE //I=512 //DMI
    bit_reverse_loop: for (int i = 0; i < SIZE; i++) {
#pragma HLS PIPELINE
#pragma HLS UNROLL factor=4
    	reversed = reverse_bits(i);	//find the bit reversed index
    	/*if(i<reversed){
    		temp=X_R[i];
    		X_R[i]=X_R[reversed];
    		X_R[reversed]=temp;

    		temp=X_I[i];
    		X_I[i]=X_I[reversed];
    		X_I[reversed]=temp;
    	}
    	OUT_R[i]=X_R[i];
    	OUT_I[i]=X_I[i];*/


    	OUT_R[i] = X_R[reversed];
    	OUT_I[i] = X_I[reversed];
   }
    //cp(X_R,  X_I,  OUT_R,  OUT_I);
}
/*=======================BEGIN: FFT=========================*/
//stage 1
void fft_stage_first(DTYPE X_R[SIZE], DTYPE X_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]) {
//Insert your code here
	float c;
	float s;
	c = W_real[0];
	s = W_imag[0];
	// Compute butterflies that use same W**k
	DFTpts_first:for(int i=0; i<SIZE; i += 2)
	{
#pragma HLS PIPELINE ii=3
#pragma HLS UNROLL factor=2
		int i_lower = i + 1;			//index of lower point in butterfly
		float temp_R = X_R[i_lower]*c;//- X_I[i_lower]*s;
		temp_R+=- X_I[i_lower]*s;
		float temp_I = X_I[i_lower]*c;//+ X_R[i_lower]*s;
		temp_I+=X_R[i_lower]*s;

		OUT_R[i_lower] = X_R[i] - temp_R;
		OUT_I[i_lower] = X_I[i] - temp_I;
		OUT_R[i] = X_R[i] + temp_R;
		OUT_I[i] = X_I[i] + temp_I;
	}
}

//stages
void fft_stages(DTYPE X_R[SIZE], DTYPE X_I[SIZE], int stage, DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]) {
#pragma HLS FUNCTION_INSTANTIATE variable=stage
/*	 #pragma HLS ARRAY_PARTITION variable=X_R block factor=8 dim=1
	#pragma HLS ARRAY_PARTITION variable=X_I block  factor=8 dim=1
	#pragma HLS ARRAY_PARTITION variable=OUT_R block  factor=8 dim=1
	#pragma HLS ARRAY_PARTITION variable=OUT_I block factor=8 dim=1*/

//Insert your code here
	float c;
	float s;
	long int loc;
	int num_sets = SIZE >> (stage);//#subDFT's
	/*Butterfly widths in a subDFT at the current stage
	(=N_{FFT at current stage}/2); also equal to number of unique
	twiddle factors in a subDFT at the current stage*/
	int num_pairs = 1 << (stage-1);
	// Compute butterflies that use same W**k
	Sets:for(int set=0; set<num_sets; set++) {
		Pairs:for(int pair=0; pair<num_pairs; pair++) {
#pragma HLS PIPELINE ii=3
#pragma HLS UNROLL factor=2
			//loc=pair*(SIZE>>stage);
			//loc=pair<<(M-stage);
			loc=(pair<<M)>>(stage);
			c = W_real[loc];
			s = W_imag[loc];


			//int i = set*(num_pairs*2) + pair;
			int i = (set << stage) + pair;
			int i_lower = i+num_pairs;

			float temp_R = X_R[i_lower]*c- X_I[i_lower]*s;
			float temp_I = X_I[i_lower]*c+ X_R[i_lower]*s;

			OUT_R[i_lower] = X_R[i] - temp_R;
			OUT_I[i_lower] = X_I[i] - temp_I;
			OUT_R[i] = X_R[i] + temp_R;
			OUT_I[i] = X_I[i] + temp_I;
		}
	}
}


//last stage
void fft_stage_last(DTYPE X_R[SIZE], DTYPE X_I[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]) {
//Insert your code here
	//Write code that computes last stage of FFT
	float c;
	float s;

	// Compute butterflies that use same W**k
	DFTpts_last:for(int i=0; i<SIZE/2; i++)
	{
		c = W_real[i];
		s = W_imag[i];
#pragma HLS PIPELINE ii=3
#pragma HLS UNROLL factor=2
		int i_lower = SIZE/2 + i;			//index of lower point in butterfly
		float temp_R = X_R[i_lower]*c- X_I[i_lower]*s;
		float temp_I = X_I[i_lower]*c+ X_R[i_lower]*s;

		OUT_R[i_lower] = X_R[i] - temp_R;
		OUT_I[i_lower] = X_I[i] - temp_I;
		OUT_R[i] = X_R[i] + temp_R;
		OUT_I[i] = X_I[i] + temp_I;
	}
}
/*=======================END: FFT=========================*/




