#include "fft.h"

static unsigned short count;
static DTYPE xr[SIZE];
static DTYPE xi[SIZE];
static DTYPE xr_out[SIZE];
static DTYPE xi_out[SIZE];


void ofdm_receiver( volatile DTYPE *inptr, volatile uint32_t *outptr )
{
//#pragma HLS PIPELINE II=32
//#pragma HLS ARRAY_PARTITION variable=xr cyclic factor=8 dim=1//complete//
//#pragma HLS ARRAY_PARTITION variable=xi cyclic factor=8 dim=1//complete//
#pragma HLS interface ap_fifo port=inptr
#pragma HLS interface ap_fifo port=outptr
#pragma HLS interface ap_ctrl_none port=return
#pragma HLS dataflow

	int   dout[SIZE];
//#pragma HLS PIPELINE II=32
	read_rx_input:
	for (int i = 0; i<SIZE;i++) {
#pragma HLS pipeline //II=32
#pragma HLS UNROLL factor=16
		xr[ i ] = *inptr++;
		xi[ i ] = *inptr++;
	}
//#pragma HLS dataflow
	count = 0;
	demod( xr, xi, dout, xr_out, xi_out );

	write_rx_output:
	for (int i = 0; i<SIZE;i++) {
#pragma HLS pipeline
#pragma HLS UNROLL factor=4
		*outptr++ = dout[i];
	}
}

void qpsk_decode(DTYPE R[SIZE], DTYPE I[SIZE], int D[SIZE]);
void demod(DTYPE X_R[SIZE], DTYPE X_I[SIZE], int D[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE])
{
#pragma HLS INLINE
	fft(X_R,X_I,OUT_R,OUT_I);
	qpsk_decode(OUT_R,OUT_I,D);
}
