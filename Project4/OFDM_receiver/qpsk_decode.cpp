#include "fft.h"
#include <stdio.h>

unsigned int reverse_bits(unsigned int input);
void qpsk_decode(DTYPE R[SIZE], DTYPE I[SIZE], int D[SIZE]) {
	//Write your code here
	for (int i=0;i<SIZE;i++) {
#pragma HLS pipeline
#pragma HLS UNROLL factor=4
		D[reverse_bits(i)] = (((R[i]<0) ? 1 : 0) | ((I[i]<0) ? 2 : 0));
		/*if (R[i]<0 && I[i]<0)
			D[i]=3;
		else if (R[i]>0 && I[i]<0)
			D[i]=2;
		else if (R[i]>0 && I[i]>0)
			D[i]=0;
		else if (R[i]<0 && I[i]>0)
			D[i]=1;
			*/
	}

}

