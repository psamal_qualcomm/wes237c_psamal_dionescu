############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project hls_ofdm_rx
set_top ofdm_receiver
add_files qpsk_decode.cpp
add_files ofdm_recvr.cpp
add_files fft.h
add_files fft.cpp
add_files -tb input.dat
add_files -tb out.gold.dat
add_files -tb ofdm_test.cpp
open_solution "solution1"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
#source "./hls_ofdm_rx/solution1/directives.tcl"
csim_design -clean
csynth_design
cosim_design
export_design -format ip_catalog
