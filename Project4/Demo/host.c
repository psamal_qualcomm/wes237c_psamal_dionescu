#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>

#define NO_SAMPLE 1024
#define NO_SAMPLE_X2 2048
#define N_ITER 1

int main(int argc, char *argv[]) {

  int fdr, fdw;

  int tologic[NO_SAMPLE_X2];
  int fromlogic[NO_SAMPLE];
  FILE * fout1;
  FILE * fout2;
  FILE * fout3;
  FILE * fout4;
  FILE * fin;

  int i;
  int j;
  int k;

  float * I;
  float * Q;
  int * D;

  I = (float*)malloc(sizeof(float)*NO_SAMPLE*N_ITER);
  Q = (float*)malloc(sizeof(float)*NO_SAMPLE*N_ITER);
  D = (int*)malloc(sizeof(int)*NO_SAMPLE);
  
  fin = fopen("input.dat","r");
  fout1 = fopen("input_i_t.txt","w");
  for (i = 0; i < NO_SAMPLE*N_ITER; i++)
  {
    fscanf(fin,"%f %f\n",&I[i],&Q[i]);
    fprintf(fout1,"%d %f\n",i,I[i]);
  }
  fclose(fin);
  fclose(fout1);

  for (k = 0; k < N_ITER; k++)
  {
    j = 0;
    fout1 = fopen("input_i.txt","w");
    fout2 = fopen("input_q.txt","w");
    for (i = 0; i < NO_SAMPLE_X2; i++,j++)
    {
      tologic[i] = *((int *)&I[j + k * NO_SAMPLE]);
      i++;
      tologic[i] = *((int *)&Q[j + k * NO_SAMPLE]);
      fprintf(fout1,"%d %f\n",j,I[j + k * NO_SAMPLE]);
      fprintf(fout2,"%d %f\n",j,Q[j + k * NO_SAMPLE]);
    } 
    fclose(fout1);
    fclose(fout2);
      
    // open read and write channels using fdr, fdw
    fdr = open("/dev/xillybus_read_32", O_RDONLY);
    fdw = open("/dev/xillybus_write_32", O_WRONLY);
    if ((fdr < 0) || (fdw < 0)) {
      perror("Failed to open Xillybus device file(s)");
      exit(1);
    }  
    
    // write data into and read data from the FPGA
    if (write(fdw, (void *) &tologic, sizeof(tologic)) <= 0) 
    {
      perror("write() failed");
      exit(1);
    }
    sleep(0.001);
    if (read(fdr, (void *) &fromlogic, sizeof(fromlogic)) <= 0) 
    {
      perror("read() failed");
      exit(1);
    }
    // when it is done reading, close the channels
    close(fdr);
    close(fdw);

    for (i = 0; i < NO_SAMPLE; i++)
    {
      D[i] = fromlogic[i];
    }

    fout3 = fopen("output_d.txt","w");
    fout4 = fopen("output_d_125.txt","w");
    for (i = 0; i < NO_SAMPLE; i++)
    {
	    fprintf(fout3,"%d %d\n",i,D[i]);
	    if (i <= 125)
	      fprintf(fout4,"%d %d\n",i,D[i]);
    }
    fclose(fout3);
    fclose(fout4);
  }

  free(I);
  free(Q);
  free(D);

  return 0;
}
