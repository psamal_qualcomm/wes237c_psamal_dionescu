#ifndef FAST_ATAN_H
#define FAST_ATAN_H

float fast_atan(float, float);
void expandr(float *, float, float);
#define PASSTHRESHATAN2 0.00001
#define PI (3.14159265358979323846)
#define PIDIV2 (1.57079632679489661923)
#endif
