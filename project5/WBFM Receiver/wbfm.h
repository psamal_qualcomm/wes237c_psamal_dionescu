#ifndef WBFM_H
#define WBFM_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define MYCOUNT  8 // it is MYCOUNT/2 complex numbers

void volk(float outVec[MYCOUNT], float inVec[MYCOUNT]);
void fir(float * result, float input[MYCOUNT/2]);
void iir(float input, float *output, int size);
float fast_atan(float y, float x);
void xillybus_wrapper(float *in, float *out);
void create_last_inf(float *last_inf, float *inf);

#endif
