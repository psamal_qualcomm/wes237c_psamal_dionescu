############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project WBFM
set_top xillybus_wrapper
add_files wbfm.h
add_files wbfm.cpp
add_files fast_atan.cpp
add_files -tb WBFM_test.cpp
add_files -tb myinput.txt
add_files -tb golden.txt
open_solution "solution1"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
#source "./WBFM/solution1/directives.tcl"
csim_design -clean
csynth_design
#cosim_design
#export_design -format ip_catalog
