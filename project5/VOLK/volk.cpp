/* VOLK */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "volk.h"

void volk(float outputVector[MYCOUNT], float inputVector[MYCOUNT])
{
	float r0, r1, i0, i1;
	//write your code here
	for(int i = 0; i <MYCOUNT-2; i+=2){
		r0 = inputVector[i];
		i0 = inputVector[i+1];
		r1 = inputVector[i+2];
		i1 = inputVector[i+3];

		outputVector[i] = (r0*r1)+(i0*i1);
		outputVector[i+1] = (r0*i1) - (i0*r1);
	}
}


