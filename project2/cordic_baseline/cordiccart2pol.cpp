#include "cordiccart2pol.h"

data_t Kvalues[NO_ITER] = {1,	0.500000000000000,	0.250000000000000,	0.125000000000000,	0.0625000000000000,	0.0312500000000000,	0.0156250000000000,	0.00781250000000000,	0.00390625000000000,	0.00195312500000000,	0.000976562500000000,	0.000488281250000000,	0.000244140625000000,	0.000122070312500000,	6.10351562500000e-05,	3.05175781250000e-05};

data_t angles[NO_ITER] = {0.785398163397448,	0.463647609000806,	0.244978663126864,	0.124354994546761,	0.0624188099959574,	0.0312398334302683,	0.0156237286204768,	0.00781234106010111,	0.00390623013196697,	0.00195312251647882,	0.000976562189559320,	0.000488281211194898,	0.000244140620149362,	0.000122070311893670,	6.10351561742088e-05,	3.05175781155261e-05};


void cordiccart2pol(data_t x, data_t y, data_t * r,  data_t * theta)
{
	// Write your code here
	int direction;
		/* Theta is the current approximation */
		data_t Theta =0;
		/* temporary variables for next x & y */
		data_t x_next, y_next;
		int itercnt = NO_ITER;


		//*r=(x==0)?(y==0?0:(y*(y>0?1:-1))): (y==0?(x*(x>0?1:-1)):);

		if (x < 0) {
			if (y<0) {// 3rd or 4th quadrant;
				data_t tmp = -y;
				y = x;
				x = tmp;

				Theta =  -PIOVER2;
			} else {//2nd or 1st quadrant;
				data_t tmp = -x;
				x = y;
				y = tmp;
				Theta =  PIOVER2;
			}
		} else if (x==0) {
			if (y == 0) {
				*r = 0;
				*theta = 0;
				return;
			}
		}


		for (int i = 0; i <itercnt;i++)
		{
			if (y < 0)
				direction = 1;
			else
				direction = -1;

			y_next = y + x * Kvalues[i] * direction;
			x_next = x - y * Kvalues[i] * direction;

			Theta -= direction * angles[i];
			y = y_next;
			x = x_next;
		}
		*theta = Theta;
		//*r = ((((ap_ufixed<17,3,AP_RND,AP_WRAP,1>)x *19899)>>15)); //0.607; // /cordic_gain;
		*r=x*0.607;// /cordic_gain;
	
}
