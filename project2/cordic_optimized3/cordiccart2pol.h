
#ifndef CORDICCART2POL_H
#define CORDICCART2POL_H
#include <ap_fixed.h>
#define NO_ITER 16
#define PIOVER2 1.5707963267948966192313216916398

//typedef int   coef_t;
//typedef float data_t;
//typedef float acc_t;
typedef ap_fixed<3,3>	coef_t;

typedef ap_fixed< 18, 3, AP_RND, AP_WRAP, 1> data_t;

typedef ap_fixed< 20, 7, AP_RND, AP_WRAP, 1> acc_t;
//typedef ap_fixed< 13, 2, AP_RND, AP_WRAP, 1> gain_div_t;
typedef ap_fixed< 13, 3, AP_RND, AP_WRAP, 1> atan2angle_t;


void cordiccart2pol(data_t x, data_t y, data_t * r,  data_t * theta);

#endif
