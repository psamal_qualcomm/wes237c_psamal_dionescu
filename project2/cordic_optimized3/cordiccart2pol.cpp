#include "cordiccart2pol.h"

data_t Kvalues[NO_ITER] = {1,	0.500000000000000,	0.250000000000000,	0.125000000000000,	0.0625000000000000,	0.0312500000000000,	0.0156250000000000,	0.00781250000000000,	0.00390625000000000,	0.00195312500000000,	0.000976562500000000,	0.000488281250000000,	0.000244140625000000,	0.000122070312500000,	6.10351562500000e-05,	3.05175781250000e-05};

atan2angle_t angles[NO_ITER] = {0.785398163397448,	0.463647609000806,	0.244978663126864,	0.124354994546761,	0.0624188099959574,	0.0312398334302683,	0.0156237286204768,	0.00781234106010111,	0.00390623013196697,	0.00195312251647882,	0.000976562189559320,	0.000488281211194898,	0.000244140620149362,	0.000122070311893670,	6.10351561742088e-05,	3.05175781155261e-05};


void cordiccart2pol(data_t x, data_t y, data_t * r,  data_t * theta)
{
	// Write your code here
	int direction;
		/* Theta is the current approximation */
		atan2angle_t Theta =0;
		/* temporary variables for next x & y */
		data_t x_next, y_next;
		int itercnt = NO_ITER;
		data_t iqarray[4];
#pragma HLS ARRAY_PARTITION variable=iqarray complete dim=1
		iqarray[0]=x;
		iqarray[1]=y;
		coef_t loc2flipsgn=y<0?1:0;//if in 3rd/4th quadrant then loc2flipsgn = 1; if in 1st/2nd quadrant or on border, then loc2flipsgn = 1
		iqarray[loc2flipsgn]*=-1;
		iqarray[2]=iqarray[0];
		iqarray[0]=iqarray[1];
		iqarray[1]=iqarray[2];
		// 3rd/4th quadrant => first rotation is by +90, 1st/2nd quadrant => first rotation is by -90
		// but note: we must account for every rotation we make by accumulating its NEGATIVE, in order to eventually yield an atan2-type angle value between -180 and +180
		Theta=loc2flipsgn==1?-PIOVER2:PIOVER2;


		data_t factor=1.0;
		for (int i = 0; i <itercnt;i++)
		{
#pragma HLS UNROLL
			direction = iqarray[1]<0 ?1:-1;

			//iqarray[3] = iqarray[1] + iqarray[0] * factor * direction;
			//iqarray[2] = iqarray[0] - iqarray[1] * factor * direction;

			//iqarray[3] = iqarray[1] + (iqarray[0] >>i) * direction;
			//iqarray[2] = iqarray[0] - (iqarray[1] >>i) * direction;


			iqarray[3] = direction==1?iqarray[1] + (iqarray[0] >>i) :iqarray[1] - (iqarray[0] >>i) ;
			iqarray[2] = direction==1?iqarray[0] - (iqarray[1] >>i) :iqarray[0] + (iqarray[1] >>i);

			Theta -= direction * angles[i];
			iqarray[1] = iqarray[3];
			iqarray[0] = iqarray[2];
			factor=factor>>1;
		}
		*theta = Theta;

		//next two are more brute force but work well; i started with former
		//*r = (((  (ap_ufixed<17,3,AP_RND,AP_WRAP,1>)iqarray[0] *19899)>>15)); //0.607; // /cordic_gain;
		//*r = (((  (((ap_ufixed<28,3,AP_RND,AP_WRAP,1>)iqarray[0])>>15) *19899))); //0.607; // /cordic_gain;

		//printf("%f\n",(float)*r);
		//printf("%f\n",(float)iqarray[0]);

		//my multiplicative order method to cancel cordic gain using only adds and shifts
		ap_ufixed<30,3,AP_RND,AP_WRAP,1> aux=(((ap_ufixed<30,3,AP_RND,AP_WRAP,1>)iqarray[0])>>15);
		ap_ufixed<30,3,AP_RND,AP_WRAP,1> auxtimeseleven=aux+(aux<<3) +(aux<<1);
		ap_ufixed<30,3,AP_RND,AP_WRAP,1> dummy=auxtimeseleven+(auxtimeseleven<<4)+(auxtimeseleven<<8)+(aux<<9)+(aux<<14);
		*r= (ap_ufixed<17,3,AP_RND,AP_WRAP,1>)dummy;
		//<><><><><><><><><><><><><> end cordic gain compensation <><><><><><><><><><<><><><>




		//*r = (((ap_ufixed<17,3,AP_RND,AP_WRAP,1>)iqarray[0])>>2) *(19899>>13); //0.607; // /cordic_gain;
		//*r = iqarray[0]*((gain_div_t)(1.0/cordic_gain));




}
