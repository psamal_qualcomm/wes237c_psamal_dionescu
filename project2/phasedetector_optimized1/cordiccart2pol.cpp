#include "phasedetector.h"

data_t Kvalues[NO_ITER] = {1,	0.500000000000000,	0.250000000000000,	0.125000000000000,	0.0625000000000000,	0.0312500000000000,	0.0156250000000000,	0.00781250000000000,	0.00390625000000000,	0.00195312500000000,	0.000976562500000000,	0.000488281250000000,	0.000244140625000000,	0.000122070312500000,	6.10351562500000e-05,	3.05175781250000e-05};

data_t angles[NO_ITER] = {0.785398163397448,	0.463647609000806,	0.244978663126864,	0.124354994546761,	0.0624188099959574,	0.0312398334302683,	0.0156237286204768,	0.00781234106010111,	0.00390623013196697,	0.00195312251647882,	0.000976562189559320,	0.000488281211194898,	0.000244140620149362,	0.000122070311893670,	6.10351561742088e-05,	3.05175781155261e-05};
//data_t cordic_gain[NO_ITER] = {1.41421, 1.58114, 1.62980, 1.64248, 1.64569, 1.64649, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669};

void cordiccart2pol(data_t x, data_t y, data_t * r,  data_t * theta)
{
	int direction;
	/* _theta is the current approximation */
	data_t _theta =0;
	/* temporary variables for next x & y */
	data_t x_next, y_next;
	int loops = 16;
	data_t cordic_gain = 1.64669;

	if (x < 0) {
		if (y<0) {
			data_t tmp = -y;
			y = x;
			x = tmp;
			//start_quadrant = 3;
			_theta =  -1.5707963267948966192313216916398;
		} else {
			data_t tmp = -x;
			x = y;
			y = tmp;
			_theta =  1.5707963267948966192313216916398;
			//start_quadrant = 2;
		}
	} else if (x==0) {
		if (y == 0) {
			*r = 0;
			*theta = 0;
			return;
		}
	}

	for (int i = 0; i <loops;i++)
	{
		if (y < 0)
			direction = 1;
		else
			direction = -1;

		y_next = y + x * Kvalues[i] * direction;
		x_next = x - y * Kvalues[i] * direction;

		_theta -= direction * angles[i];
		y = y_next;
		x = x_next;
	}
	*theta = _theta;
	*r = x/cordic_gain;
}
