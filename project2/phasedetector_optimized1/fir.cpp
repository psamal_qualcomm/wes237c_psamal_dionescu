/*
	Filename: fir.cpp
		Complex FIR or Match filter
		firI1 and firI2 share coef_t c[N]
		firQ1 and firQ2 share coef_t c[N]
		
	INPUT:
		I: signal for I sample
		I: signal for Q sample

	OUTPUT:
		X: filtered output
		Y: filtered output

*/

#include "phasedetector.h"

static void inline fir_internal(
		  data_t *y,
		  data_t x,
		  data_t shift_reg[N],
		  const coef_t c[N]
		  )
{
	#pragma HLS ARRAY_PARTITION variable=shift_reg complete factor=1 dim=1

	acc_t acc;

	data_t data;
	int i;
	acc_t mult;


	mult = x * c[0];
	acc=mult;


	Accum_Loop: for (i=N-2;i>=0;i--) {
#pragma HLS UNROLL
			data = shift_reg[i];
			mult=data*c[i+1];
			acc+=mult;
	}



	Shift_Loop: for (i=N-2;i>0;i--) {
#pragma HLS UNROLL
			data = shift_reg[i-1];
			shift_reg[i] = data;
	}
	shift_reg[0] = x;
	*y=acc;
}

void firI1 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};
	static data_t shift_reg[N];
	fir_internal(y,x,shift_reg,c);
}


void firI2 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};
	static data_t shift_reg[N];
	fir_internal(y,x,shift_reg,c);
}




void firQ1 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};
	static data_t shift_reg[N];
	fir_internal(y,x,shift_reg,c);
}

void firQ2 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};
	static data_t shift_reg[N];
	fir_internal(y,x,shift_reg,c);
	// Write your code here
	
}


void fir (
  data_t I,
  data_t Q,

  data_t *X,
  data_t *Y
  ) {

	data_t IinIfir, QinQfir, IinQfir, QinIfir;
	// Write your code here
	
	//Calculate X
	firI1(&IinIfir,I);
	firI2(&QinIfir,Q);
	firQ1(&QinQfir,Q);
	firQ2(&IinQfir,I);
	*X = IinIfir+QinQfir;
	*Y = QinIfir-IinQfir;

	//Calculate Y



}


