/*
	Filename: fir.cpp
		Complex FIR or Match filter
		firI1 and firI2 share coef_t c[N]
		firQ1 and firQ2 share coef_t c[N]
		
	INPUT:
		I: signal for I sample
		I: signal for Q sample

	OUTPUT:
		X: filtered output
		Y: filtered output

*/

#include "phasedetector.h"

static void inline shift_in(
	  data_in_t x,
	  data_in_t shift_reg[N]
	)
{
#pragma HLS INLINE
	Shift_Loop: for (int i=N-2;i>0;i--) {
#pragma HLS UNROLL
		shift_reg[i] = shift_reg[i-1];
	}
	shift_reg[0] = x;
}

static void inline fir_internal(
		  acc_t *y,
		  data_in_t x,
		  const data_in_t shift_reg[N],
		  const coef_t c[N]
		  )
{
#pragma HLS INLINE
	//hls was ignoring the inline part of the function declaration.
#pragma HLS INLINE
	#pragma HLS ARRAY_PARTITION variable=shift_reg complete factor=1 dim=1

	acc_t acc;
	data_in_t mult;


	mult = x * c[0];
	acc=mult;

	Accum_Loop: for (int i=N-2;i>=0;i--) {
#pragma HLS UNROLL
			mult=shift_reg[i]*c[i+1];
			acc+=mult;
	}

	*y = acc;
}

void fir (
  data_t I,
  data_t Q,

  data_t *X,
  data_t *Y
  ) {

#pragma HLS PIPELINE

	coef_t cI[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};
	coef_t cQ[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};
	static data_in_t shift_reg_I[N];
	static data_in_t shift_reg_Q[N];
	acc_t IinIfir, QinQfir, IinQfir, QinIfir;
	// Write your code here
	
	//Calculate X
	fir_internal(&IinIfir,I,shift_reg_I,cI);
	fir_internal(&QinIfir,Q,shift_reg_Q,cI);
	fir_internal(&QinQfir,Q,shift_reg_Q,cQ);
	fir_internal(&IinQfir,I,shift_reg_I,cQ);

	shift_in(I,shift_reg_I);
	shift_in(Q,shift_reg_Q);

	*X = IinIfir+QinQfir;
	*Y = QinIfir-IinQfir;

	//Calculate Y



}


