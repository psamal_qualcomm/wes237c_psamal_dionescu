/*
	Filename: fir.h
		Header file
		FIR lab wirtten for 237C class at UCSD.

*/
#ifndef PHASE_DETECTOR_H_
#define PHASE_DETECTOR_H_


#include <ap_fixed.h>

const int N = 32;

#define NO_ITER 16

typedef int   coef_t;
typedef float data_t;
typedef ap_fixed< 15, 2, AP_RND, AP_WRAP, 1> data_in_t;
typedef ap_fixed< 20, 7, AP_RND, AP_WRAP, 1> acc_t;

typedef ap_fixed< 17, 2, AP_RND, AP_WRAP, 1> gain_div_t;
typedef ap_fixed< 14, 3, AP_RND, AP_WRAP, 1> angle_t;
typedef ap_fixed< 21, 8, AP_RND, AP_WRAP, 1> coordinate_t;

void phasedetector (
  data_t I,
  data_t Q,

  data_t *R,
  data_t *theta
  );

void cordiccart2pol(data_t x, data_t y, data_t * r, data_t * theta);

void fir (
  data_t I,
  data_t Q,

  data_t *X,
  data_t *Y
  );

#endif
