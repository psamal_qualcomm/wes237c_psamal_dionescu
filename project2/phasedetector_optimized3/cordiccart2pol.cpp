#include "phasedetector.h"

coordinate_t Kvalues[NO_ITER] = {1,	0.500000000000000,	0.250000000000000,	0.125000000000000,	0.0625000000000000,	0.0312500000000000,	0.0156250000000000,	0.00781250000000000,	0.00390625000000000,	0.00195312500000000,	0.000976562500000000,	0.000488281250000000,	0.000244140625000000,	0.000122070312500000,	6.10351562500000e-05,	3.05175781250000e-05};

angle_t angles[NO_ITER] = {0.785398163397448,	0.463647609000806,	0.244978663126864,	0.124354994546761,	0.0624188099959574,	0.0312398334302683,	0.0156237286204768,	0.00781234106010111,	0.00390623013196697,	0.00195312251647882,	0.000976562189559320,	0.000488281211194898,	0.000244140620149362,	0.000122070311893670,	6.10351561742088e-05,	3.05175781155261e-05};
//data_t cordic_gain[NO_ITER] = {1.41421, 1.58114, 1.62980, 1.64248, 1.64569, 1.64649, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669, 1.64669};

void cordiccart2pol(data_t x_in, data_t y_in, data_t * r,  data_t * theta)
{
#pragma HLS PIPELINE
	int direction;
	/* _theta is the current approximation */
	angle_t _theta =0;
	/* temporary variables for next x & y */
	coordinate_t x,y, x_next, y_next;
	coordinate_t x_diff, y_diff;
	x = x_in;
	y = y_in;
	int loops = 11;
	float cordic_gain = 1.64669;

	if (x < 0) {
		/* multiplication by -1 was needed to satisfy type system. */
		coordinate_t tmp = (y<0) ? y*(-1) : y*1;
		y = (y<0) ? x*1 : x*(-1);
		x = tmp;
		_theta = (y<0) ? -1.5707963267948966192313216916398 : 1.5707963267948966192313216916398;
	} /* else if (x==0) {
		if (y == 0) {
			*r = 0;
			*theta = 0;
			return;
		}
	} */


	for (int i = 0; i <loops;i++)
	{
		x_diff = x >> i;
		y_diff = y >> i;

		direction = (y < 0)? 1 : -1;

		y_next = y + x_diff * direction;
		x_next = x - y_diff * direction;

		_theta -= direction * angles[i];
		y = y_next;
		x = x_next;
	}
	*theta = _theta;

	*r = x*((gain_div_t)(1.0/cordic_gain));
	//*r = (x>>1) + (x>>7) + (x>>6) + (x>>5);
}
