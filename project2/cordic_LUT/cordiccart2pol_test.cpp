#define TEST_BENCH

#include "cordiccart2pol.h"

#include <math.h>
#include <stdio.h>
#include <ap_fixed.h>

/// Struct to calculate the error
struct Rmse
{
	int num_sq;
	float sum_sq;
	float error;

	Rmse(){ num_sq = 0; sum_sq = 0; error = 0; }

	float add_value(float d_n)
	{
		num_sq++;
		sum_sq += (d_n*d_n);
		error = sqrtf(sum_sq / num_sq);
		return error;
	}

};

Rmse rmse_theta;
Rmse rmse_r;

int main()
{

	
	//setup step size to sweep a 12 bit bipolar adc input
	float step = 1.0/2048;
	int num_steps = (int) 2/step; // x and y are normalized and are between -1 and 1
	
	initialize_lut();


	/* This part fills the LUTs partially*/
/*
	data_t _x;
	data_t _y;

	for(int i = 0; i < num_steps+1; i++)
	{
		for(int j = 0; j < num_steps+1; j++)
		{
			_x = -1 + i*step;
			_y = -1 + j*step;

			ap_fixed<W, I, AP_RND_ZERO, AP_WRAP, 1> fixed_x = _x;
			ap_fixed<W, I, AP_RND_ZERO, AP_WRAP, 1> fixed_y = _y;

			ap_uint<2*(I+MAN_BITS)> index;

			for(int i = 0; i < I+MAN_BITS; i++)
			{
				index[2*(I+MAN_BITS)-1-i] = fixed_x[W-1-i];
				index[(I+MAN_BITS)-1-i]   = fixed_y[W-1-i];
			}

			if((_x == 0) & (_y == 0)){
				my_LUT_th[index] = 0;
				my_LUT_r[index]  = 0;
			}
			else{
				my_LUT_th[index] = atan2f(_y, _x);
				my_LUT_r[index]  = sqrtf((_y*_y)+(_x*_x));
			}
		}
	}
*/
	// This part test your code against the ground truth
	//
	data_t x;
	data_t y;
	data_t r;
	data_t theta;

	//step += ERROR; // introduce small error to avoid testing the exact numbers from the LUT

	for(int i = 0; i < num_steps+1; i++)
	{
		for(int j = 0; j < num_steps+1; j++)
		{
			x = -1 + i*step + ERROR;
			y = -1 + j*step + ERROR;
			//printf("testing %f, %f\n", x, y);
			/* require minimum signal strength for valid detection */
			/* fixed with second finer grain lut. */
/*			if(sqrtf(x*x)<1.0/256 && sqrtf(y*y)<1.0/256) {
				printf("skipping %f, %f\n", x, y);
				continue;
			}
*/
			// Run the Cordic code
			cordiccart2pol(x, y, &r, &theta);
			float golden_r, golden_th;

			if((x == 0) & (y == 0)){
				golden_r  = 0;
				golden_th = 0;
			}
			else{
				golden_r  = sqrtf((y*y) + (x*x));
				golden_th = atan2f(y, x);
			}

			/* tollerate wrap-arround & report acute angles*/
			float theta_err = (float)theta - golden_th;
			if (theta_err > 3.141592654)
				theta_err -=2*3.141592654;
			else if (theta_err < -3.141592654)
				theta_err += 2*3.141592654;

			float r_err = (float)r - golden_r;
			// Calculate error from ground truth
			rmse_theta.add_value(theta_err);
			r_err = rmse_r.add_value(r_err);

			float tollerance = .01;

			int should_print = (theta_err>tollerance) || (theta_err<-tollerance) || (r_err>tollerance) ||(r_err<-tollerance);
			should_print = 0;
			if (should_print)
				printf("input: x=%.4f, y=%.4f\n"
					"true results: R=%.4f, Theta=%.4f\n"
					"your results: R=%.4f, Theta=%.4f\n"
					"theta_err %.4f\n",
					(float)x, (float)y,
					golden_r, golden_th,
					(float)r, (float)theta,
					theta_err);
		}
	}


	printf("   RMSE(R)           RMSE(Theta)\n");
	printf("%0.15f %0.15f\n", rmse_r.error, rmse_theta.error);

	float error_threshold = 0.01;

    int success = (rmse_r.error < error_threshold) && (rmse_theta.error < error_threshold);

    if (success) return 0;
    else return 1;
}
