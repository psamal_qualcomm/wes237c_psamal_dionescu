#include "cordiccart2pol.h"
// Look-up tables.
// They are filled in the test bench. You could also explicitly write all the numbers.
volatile data_t my_LUT_th[LUT_SIZE];
volatile data_t my_LUT_r[LUT_SIZE];

void initialize_lut()
{
	/* This part fills the LUTs completely*/
	// This part creates the LUTs
	//printf("lutsize = %d",LUT_SIZE);
	for(int i=0; i<LUT_SIZE; i++){

		ap_uint<2*(I+MAN_BITS)> index = i;
		ap_fixed<I+MAN_BITS, I, AP_RND, AP_WRAP, 1> fixed_x;
		ap_fixed<I+MAN_BITS, I, AP_RND, AP_WRAP, 1> fixed_y;

		for(int j = 0; j < I+MAN_BITS; j++)
			{
				fixed_x[I+MAN_BITS-1-j] = index[2*(I+MAN_BITS)-1-j];
				fixed_y[I+MAN_BITS-1-j] = index[(I+MAN_BITS)-1-j];
			}

		float _x = fixed_x;
		float _y = fixed_y;
/*		if (i%4 == 0)
			printf("\n");
		printf("%d:%f,%f ",i,_x, _y);
*/

		if((_x == 0) & (_y == 0)){
			my_LUT_th[index] = 0;
			my_LUT_r[index]  = 0;
		}
		else{
			my_LUT_th[index] = atan2f(_y, _x);
			my_LUT_r[index]  = sqrtf((_y*_y)+(_x*_x));
		}
	}
	//printf("\nlut_initialized\n");
}



void cordiccart2pol(data_t x_in, data_t y_in, data_t * r,  data_t * theta)
{
#pragma HLS RESOURCE variable=my_LUT_r core=RAM_1P_LUTRAM
#pragma HLS RESOURCE variable=my_LUT_th core=RAM_1P_LUTRAM
	// you have to answer to this question: What happens if you uncomment these pragma?
//#pragma HLS RESOURCE variable=my_LUT_th core=RAM_1P_LUTRAM
//#pragma HLS RESOURCE variable=my_LUT_r core=RAM_1P_LUTRAM

	int scale =0;
	data_t x = x_in;
	data_t y = y_in;


/*	float cutoff = (1./ (1<<(MAN_BITS-1)));
	if ((x< cutoff) && (x > -cutoff) && (y < cutoff) && (y > -cutoff )) {
		x *= 1<<(MAN_BITS-1);
		y *= 1<<(MAN_BITS-1);
		scale++;
	}
*/
	// Convert the floating-point inputs to fixed-point representation
	ap_fixed<I+MAN_BITS, I, AP_RND, AP_WRAP, 1> fixed_x = x;
	ap_fixed<I+MAN_BITS, I, AP_RND, AP_WRAP, 1> fixed_y = y;

	// Build the index to find the entries in the LUT.
	ap_uint<2*(I+MAN_BITS)> index;


	// Concatenate the bits of (fixed-point) x and y to create the index
	for(int i = 0; i < I+MAN_BITS; i++)
	{
#pragma HLS UNROLL
		index[2*(I+MAN_BITS)-1-i] = fixed_x[(I+MAN_BITS)-1-i];
		index[(I+MAN_BITS)-1-i]   = fixed_y[(I+MAN_BITS)-1-i];
	}

/*	int tmp = index;
	float tmp_x = fixed_x;
	float tmp_y = fixed_y;
	if (scale)/*(x < 0 && fixed_y !=0)*/
/*		printf("index %d: %f, %f, scale %d, was %f, %f, cutoff %f\n", tmp, tmp_x, tmp_y, scale,x_in,y_in, cutoff);
*/	// Get the result from the LUT and write it back to the output
	float tmp_r = my_LUT_r[index];
	*r     = scale ? tmp_r * (1./ (1<<((MAN_BITS-1)))) : tmp_r;
	*theta = my_LUT_th[index];
}
