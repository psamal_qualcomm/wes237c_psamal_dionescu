/*
	Filename: fir.cpp
		Complex FIR or Match filter
		firI1 and firI2 share coef_t c[N]
		firQ1 and firQ2 share coef_t c[N]
		
	INPUT:
		I: signal for I sample
		I: signal for Q sample

	OUTPUT:
		X: filtered output
		Y: filtered output

*/

#include "fir.h"

void firI1 (
	acc_t *y,//data_t *y,
	data_t x
  )
{
#pragma HLS PIPELINE
#pragma HLS INTERFACE ap_vld port=x

	coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};
	// Write your code here
#pragma HLS ARRAY_PARTITION variable=c complete dim=1
	

	static data_t shift_reg[N] = {0}; //filter's state
#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=32 dim=1
		acc_t accumulate = 0;
		data_t data = 0;


		fir_slide_window: for (int i = N-1; i>= 1; i--)
				{
#pragma HLS PIPELINE
			data=shift_reg[i-1];
			shift_reg[i] = data;//shift_reg[i-1];
			accumulate += data*c[i];
			}

			// method 1 of eliminating if/else ////
			shift_reg[0]=x;
			//data = x;
			accumulate += x*c[0];//accumulate += data*c[0];
			///////////// end method 1 ////////////

			*y = accumulate;
}


void firI2 (
	acc_t *y,//data_t *y,
	data_t x
  )
{
#pragma HLS PIPELINE
#pragma HLS INTERFACE ap_vld port=x

	coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};
	// Write your code here
#pragma HLS ARRAY_PARTITION variable=c complete dim=1


	static data_t shift_reg[N] = {0}; //filter's state
#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=32 dim=1
		acc_t accumulate = 0;
		data_t data = 0;


		fir_slide_window: for (int i = N-1; i>= 1; i--)
				{
#pragma HLS PIPELINE
			data=shift_reg[i-1];
			shift_reg[i] = data;//shift_reg[i-1];
			accumulate += data*c[i];
			}

			// method 1 of eliminating if/else ////
			shift_reg[0]=x;
			//data = x;
			accumulate += x*c[0];//accumulate += data*c[0];
			///////////// end method 1 ////////////

			*y = accumulate;
	
}




void firQ1 (
	acc_t *y,//data_t *y,
	data_t x
  )
{
#pragma HLS PIPELINE
#pragma HLS INTERFACE ap_vld port=x

	coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};
	// Write your code here
#pragma HLS ARRAY_PARTITION variable=c complete dim=1


	static data_t shift_reg[N] = {0}; //filter's state
#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=32 dim=1
		acc_t accumulate = 0;
		data_t data = 0;


		fir_slide_window: for (int i = N-1; i>= 1; i--)
				{
#pragma HLS PIPELINE
			data=shift_reg[i-1];
			shift_reg[i] = data;//shift_reg[i-1];
			accumulate += data*c[i];
			}

			// method 1 of eliminating if/else ////
			shift_reg[0]=x;
			//data = x;
			accumulate += x*c[0];//accumulate += data*c[0];
			///////////// end method 1 ////////////

			*y = accumulate;
	
}

void firQ2 (
	acc_t *y,//data_t *y,
	data_t x
  )
{
#pragma HLS PIPELINE
#pragma HLS INTERFACE ap_vld port=x

	coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};
	// Write your code here
#pragma HLS ARRAY_PARTITION variable=c complete dim=1


	static data_t shift_reg[N] = {0}; //filter's state
#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=32 dim=1
		acc_t accumulate = 0;
		data_t data = 0;


		fir_slide_window: for (int i = N-1; i>= 1; i--)
				{
#pragma HLS PIPELINE
			data=shift_reg[i-1];
			shift_reg[i] = data;//shift_reg[i-1];
			accumulate += data*c[i];
			}

			// method 1 of eliminating if/else ////
			shift_reg[0]=x;
			//data = x;
			accumulate += x*c[0];//accumulate += data*c[0];
			///////////// end method 1 ////////////

			*y = accumulate;
	
}


void fir (
  data_t I,
  data_t Q,

  acc_t *X,
  acc_t *Y
  )
{
	acc_t ox1, ox2, oy1, oy2;

	// Write your code here
	
	//Calculate X
    firI1(&ox1, I);
    firQ1(&ox2, Q);

	//Calculate Y
    firI2(&oy1,Q);
    firQ2(&oy2,I);

    *X = ox1 + ox2;
    *Y = oy1 - oy2;



}


