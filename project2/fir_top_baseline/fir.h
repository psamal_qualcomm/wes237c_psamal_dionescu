/*
	Filename: fir.h
		Header file
		FIR lab wirtten for 237C class at UCSD.

*/
#ifndef FIR_H_
#define FIR_H_

#include <ap_fixed.h>

const int N = 32;
//Define fixed point widths
#define W_IN 14// 9, vs. 16 across the board, improves mux 72 -> 58; FF 96 -> 89; ap_clk 8.52 -> 8.34;
#define IW_IN 2
#define W_COEF 2
#define IW_COEF 2
#define W_OUT 19 //24
#define IW_OUT 7 //24




			//typedef int   coef_t;
			//typedef float data_t;
			//typedef ap_fixed< 14, 2, AP_RND, AP_WRAP, 1> data_in_t;
			//typedef ap_fixed< 19, 7, AP_RND, AP_WRAP, 1> acc_t;




typedef ap_fixed<W_COEF,IW_COEF>	coef_t;
//typedef float data_t;//HH
typedef ap_fixed<W_IN,IW_IN,AP_RND,AP_WRAP,1>	data_t; //from old Proj1
typedef ap_fixed<W_OUT,IW_OUT,AP_RND,AP_WRAP,1>	acc_t;

/*
typedef int	  coef_t;
typedef float data_t;
typedef float acc_t;
*/

void firI1 (
  acc_t *y,//data_t *y,
  data_t x
  );
void firI2 (
  acc_t *y,//data_t *y,
  data_t x
  );
void firQ1 (
  acc_t *y,//data_t *y,
  data_t x
  );
void firQ1 (
  acc_t *y,//data_t *y,
  data_t x
  );
void fir (
  data_t I,
  data_t Q,

  acc_t *X,
  acc_t *Y
  );



#endif
