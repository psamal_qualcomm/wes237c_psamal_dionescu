/*
	Filename: fir.cpp
		FIR lab wirtten for WES/CSE237C class at UCSD.
		Match filter
	INPUT:
		x: signal (chirp)

	OUTPUT:
		y: filtered output

*/

#include "fir.h"

void fir (
  acc_t *y,//data_t *y,
  data_t x
  )
{
#pragma HLS PIPELINE //can coexist with pipeline pragma for loop level
#pragma HLS INTERFACE ap_vld port=x

	coef_t c[N] = {10, 11, 11, 8, 3, -3, -8, -11, -11, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -11, -11, -8, -3, 3, 8, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 8, 3, -3, -8, -11, -11, -10, -10, -10, -10, -10, -10, -10, -10, -11, -11, -8, -3, 3, 8, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 8, 3, -3, -8, -11, -11, -10, -10, -10, -10, -10, -10, -10, -10, -11, -11, -8, -3, 3, 8, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
#pragma HLS ARRAY_PARTITION variable=c complete dim=1
	
	// Write your code here
	static data_t shift_reg[N] = {0}; //filter's state
#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=32 dim=1
		acc_t accumulate = 0;
		data_t data = 0;


		fir_slide_window: for (int i = N-1; i>= 1; i--)
				{
#pragma HLS PIPELINE
					//if ( i == 0)
					//{
					//	shift_reg[0]=x;
					//	data = x;
					//}
					//else
					//{
						//shift_reg[i] = shift_reg[i-1];
						//data=shift_reg[i];
						data=shift_reg[i-1];
						shift_reg[i] = data;//shift_reg[i-1];

					//}
				accumulate += data*c[i];
				}

				// method 1 of eliminating if/else ////
				shift_reg[0]=x;
				//data = x;
				accumulate += x*c[0];//accumulate += data*c[0];
				///////////// end method 1 ////////////

				*y = accumulate;
}


