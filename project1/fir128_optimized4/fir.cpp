/*
	Filename: fir.cpp
		FIR lab wirtten for WES/CSE237C class at UCSD.
		Match filter
	INPUT:
		x: signal (chirp)

	OUTPUT:
		y: filtered output

*/

#include "fir.h"

void fir (
  data_t *y,
  data_t x
  )
{

	coef_t c[N] = {10, 11, 11, 8, 3, -3, -8, -11, -11, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -11, -11, -8, -3, 3, 8, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 8, 3, -3, -8, -11, -11, -10, -10, -10, -10, -10, -10, -10, -10, -11, -11, -8, -3, 3, 8, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 8, 3, -3, -8, -11, -11, -10, -10, -10, -10, -10, -10, -10, -10, -11, -11, -8, -3, 3, 8, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
	
	// Write your code here
	//assuming that N is even
	static data_t shift_reg[N] = {0};
		acc_t accumulate = 0;
		data_t data = 0;


		 fir_1stpartitioned:for (int i = N-1; i >= N/2; i--)
		 {
		  shift_reg[i] = shift_reg[i-1];
		  data = shift_reg[i];
		  accumulate += data*c[i];
		 }

		 fir_2ndpartitioning:for (int i = N/2-1; i >= 1; i--)
		 {
		  //if(i == 0)
		  //{
		  // shift_reg[0]= x;
		  // data = x;
		  //}
		  //else
		  //{
		   shift_reg[i] = shift_reg[i-1];
		   data = shift_reg[i];
		  //}
		  accumulate += data*c[i];
		 }
		 shift_reg[0]=x;
		 data = x;
		 accumulate += data*c[0];

	*y = accumulate;
}


