/*
	Filename: fir.h
		Header file
		FIR lab written for WES/CSE237C class at UCSD.

*/


#ifndef FIR_H_
#define FIR_H_

#include <ap_int.h>

const int N=128;
//Define fixed point widths
#define W_IN 9 // 9, vs. 16 across the board, improves mux 72 -> 58; FF 96 -> 89; ap_clk 8.52 -> 8.34;
#define IW_IN 9
#define W_COEF 5
#define IW_COEF 5
#define W_OUT 16
#define IW_OUT 16



/*
typedef int	coef_t;
typedef int	data_t;
typedef int	acc_t;
*/

typedef ap_fixed<W_COEF,IW_COEF>	coef_t;
typedef ap_fixed<W_IN,IW_IN>	data_t;
typedef ap_fixed<W_OUT,IW_OUT>	acc_t;


void fir (
  acc_t *y,//data_t *y,
  data_t x
  );

#endif
